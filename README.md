# README #

srch is a code search tool plus other useful things.

### What is this repository for? ###

* Search code with nice color highlighting.

### How do I get set up? ###

* Clone the repo
* Create symbolic links to srch.py and fnd.py
    * ~/bin $ ln -s /home/<you>/<sourcepath>/srch/py/srch.py srch
    * ~/bin $ ln -s /home/<you>/<sourcepath>/srch/py/fnd.py fnd
* Search code with "$ srch something something else"
* Find files with "$ fnd something something else"
