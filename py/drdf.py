#!/usr/bin/python
"""Simple directory diff tool

Sometimes it is useful to diff directories and get info about which files have
the same name, but are different, or are the same, but have moved, and similar
situations.
"""

import pipe
import os

__author__ = "Anders Clausen"
__copyright__ = "Copyright 2015 Anders Clausen"
__license__ = "GPLv3"
__version__ = "0.0.1"
__maintainer__ = "Anders Clausen"

LICENSE = '''
    drdf.py -- a simple directory diff tool.
    Copyright (C) 2015 Anders Clausen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License at <http://www.gnu.org/licenses/>
    for more details.
'''


text_chars = bytearray(set([7, 8, 9, 10, 12, 13, 27]) |
                       set(range(0x20, 0x100)) - set([0x7f]))


class Col:
    def __init__(self):
        pass


def is_binary(file_name):
    """
    Check if file is binary. Solution from http://stackoverflow.com/a/7392391
    :param file_name: The file to check
    :return: True if guessed to be binary, False otherwose
    """
    with open(file_name, 'rb') as bf:
        return bool(bf.read(1024).translate(None, text_chars))


def usage():
    print "Usage: drdf.py <directory 1> <directory 2>"
    exit(1)


def traverse_folder(path, options, the_args):
    options.path = path
    collect_filter = pipe.main(options_provided=options,
                               the_args_provided=the_args, collect=True)

    for item in collect_filter.result:
        abspath = os.path.abspath(item['joined'])
        item['abspath'] = abspath
        item['is_binary'] = is_binary(abspath)

    return collect_filter.result


def get_dicts(directory):
    hash_dict_result = {}
    file_dict_result = {}

    for item in directory:
        file_hash = item['file_hash']
        if hash_dict_result.has_key(file_hash):
            hash_dict_result[file_hash].append(item)
        else:
            hash_dict_result[file_hash] = [item]

        file_name = item['file_name']
        if file_dict_result.has_key(file_name):
            file_dict_result[file_name].append(item)
        else:
            file_dict_result[file_name] = [item]

    return hash_dict_result, file_dict_result


def get_key_sets(one, two):
    keys_one = set(one.keys())
    keys_two = set(two.keys())

    in_one_but_not_in_two = keys_one - keys_two
    in_two_but_not_in_one = keys_two - keys_one
    in_both = keys_one & keys_two

    return in_one_but_not_in_two, in_two_but_not_in_one, in_both


def print_one_but_not_other(key_set, dictionary, name1, name2, detail):
    print col("--------[ In %s but not in %s ]--------" % (name1, name2),
        Col.Green)
    for key in key_set:
        item_list = dictionary[key]
        for f1 in item_list:

            binary = f1['is_binary']
            print "%s file %s is in %s, but not in %s. Size: %s" %\
                  (col("Binary" if binary else "Text", Col.Cyan),
                   col(f1['file_name'], Col.Red),
                   col(name1, Col.Blue), col(name2, Col.Blue),
                   col(f1['file_size'], Col.Red))

            if detail:
                print 'Path: "%s"' % col(f1['joined'], Col.Yellow)
                print ""

    print ""

def do_file_diff(f1, f2):
    try:
        pipe.diff([f1['abspath'], f2['abspath']])
    except pipe.CommandException, ce:
        print ce.message[0]



def col(text, color):
    return "%s%s%s" % (color, text, Col.EndColor)


def print_diff_files(f1, f2):
    binary = f1['is_binary']
    print "%s file %s (%s). Size 1: %s, size 2: %s" % (col("Binary" if
        binary else "Text", Col.Cyan),
        col(f1['file_name'], Col.Red),
        col(f2['file_name'], Col.Blue),
        col(f1['file_size'], Col.Red),
        col(f2['file_size'], Col.Red))

    print 'Path 1: "%s"' % col(f1['joined'], Col.Yellow)
    print 'Path 2: "%s"' % col(f2['joined'], Col.Yellow)

    print ""


def print_diffing(both, file_dict_dir1, file_dict_dir2, diff, cmp, what):
    print col("--------[ %s ]--------" % what, Col.Blue)
    for key in both:
        files1 = file_dict_dir1[key]
        files2 = file_dict_dir2[key]
        for f1 in files1:
            f1_hash = f1[cmp]
            for f2 in files2:
                f2_hash = f2[cmp]
                if f1_hash != f2_hash:
                    print_diff_files(f1, f2)
                    if not f1['is_binary'] and diff:
                        do_file_diff(f1, f2)


def print_duplicates(hash_dict_dir, what):
    print col("--------[ %s ]--------" % what, Col.Blue)
    for k, v in hash_dict_dir.iteritems():
        if len(v) > 1:
            print col("Duplicates:", Col.Green)
            for item in v:
                print "%s @ %s" % (col(item['file_name'], Col.Red),
                    col(item['joined'], Col.Yellow))
            print "========"


def set_color_object(use_colors):
    for color in pipe.ShellColors.__dict__:
        if color.startswith('__'):
            continue
        Col.__dict__[color] = pipe.ShellColors.__dict__[color] if \
            use_colors else ''


def main():
    parser = pipe.get_options_parser()
    options, the_args = pipe.load_options(parser)

    diff = options.diff
    options.diff = False
    detail = options.detail
    options.detail = False

    set_color_object(not pipe.IS_DOS and not options.nocolors)

    if len(the_args) != 2:
        usage()

    p1 = the_args[0]
    p2 = the_args[1]
    the_args = []

    if not os.path.isdir(p1) or not os.path.isdir(p2):
        usage()

    options.md5 = True
    options.fnd = True
    options.quiet = True
    options.size = "0"

    if options.pipeinfo:
        pipe.main(options_provided=options, the_args_provided=the_args,
                  collect=True)
        exit(0)

    dir1 = traverse_folder(p1, options, the_args)
    dir2 = traverse_folder(p2, options, the_args)

    hash_dict_dir1, file_dict_dir1 = get_dicts(dir1)
    hash_dict_dir2, file_dict_dir2 = get_dicts(dir2)

    hashes_one_but_not_two, hashes_two_but_not_one, hashes_both = get_key_sets(
        hash_dict_dir1, hash_dict_dir2)

    files_one_but_not_two, files_two_but_not_one, files_both = get_key_sets(
        file_dict_dir1, file_dict_dir2)

    print_one_but_not_other(files_one_but_not_two, file_dict_dir1, "first",
        "second", detail)

    print_one_but_not_other(files_two_but_not_one, file_dict_dir2, "second",
        "first", detail)

    print_diffing(files_both, file_dict_dir1, file_dict_dir2, diff,
        'file_hash', "Diffing Files")

    print_diffing(hashes_both, hash_dict_dir1, hash_dict_dir2, False,
        'file_name', "Possibly Renamed Files")

    print_duplicates(hash_dict_dir1, "Possible Duplicates In First")
    print_duplicates(hash_dict_dir2, "Possible Duplicates In Second")



if __name__ == '__main__':
    main()
