#!/usr/bin/python
LICENSE = '''
    srch.py -- a launcher for simple pipe based text (code) search tool.
    Copyright (C) 2014 Anders Clausen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License at <http://www.gnu.org/licenses/>
    for more details.
'''
import pipe
pipe.main()